<?php

/**
 * @file
 * Provides hooks and alters for ocr_image.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function ocr_image_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.ocr_image':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The module brings the power of Optical Character Recognition (OCR) to Drupal websites. It allows you to extract text from images and PDFs, making their content searchable, editable, and more accessible.');
      $output .= '<ul><li>' . t('Text extraction from common image formats like JPG, PNG, TIFF as well as PDF documents. The extracted text can be stored and manipulated within Drupal') . '</li>';
      $output .= '<li>' . t('Integration with Views for searching and filtering content based on text extracted from images. No need for external OCR services') . '</li>';
      $output .= '<li>' . t('Improved accessibility as alternative text can be generated automatically from OCR images') . '</li>';
      $output .= '<li>' . t('Support for multiple languages using available OCR engines like Tesseract.') . '</li>';
      $output .= '<li>' . t('A robust set of APIs and hooks to leverage OCR capabilities throughout the site') . '</li>';
      $output .= '</ul>';
      return $output;

    default:
      break;
  }
}
