<?php

namespace Drupal\ocr_image;

/**
 * Url parser service interface.
 */
interface DocParserInterface {

  /**
   * {@inheritDoc}
   */
  public function getText(string $file_path, string $lang = 'eng', int $limit = 0, array $options = []);

}
