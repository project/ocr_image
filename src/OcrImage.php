<?php

namespace Drupal\ocr_image;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ocr_image\Plugin\Field\TesseractLanguages;
use Symfony\Polyfill\Intl\Normalizer\Normalizer;
use thiagoalessio\TesseractOCR\TesseractOCR;

/**
 * OCR image service.
 */
class OcrImage implements DocParserInterface {

  use StringTranslationTrait;

  /**
   * Constructor for OcrImage service.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Logger.
   */
  public function __construct(protected LoggerChannelFactoryInterface $loggerFactory) {
  }

  /**
   * Returns string from image.
   *
   * @param string $file_path
   *   The path for the image to extract text from.
   * @param string $lang
   *   Language to ocr.
   * @param int $limit
   *   Limited characters.
   * @param array $options
   *   Options.
   *
   * @return array
   *   The array string line text.
   */
  public function getText(string $file_path, string $lang = 'eng', int $limit = 0, array $options = []) {
    $ocr = new TesseractOCR($file_path);
    $data = '';
    $text = [];

    if (empty($lang)) {
      $lang = TesseractLanguages::tesseractConvertLang();
    }

    try {
      $dpi = $options['dpi'] ?? 300;
      $data = $ocr->lang($lang)->dpi($dpi)->run();
      if ($limit > 0) {
        $data = substr($data, 0, $limit);
      }
    }
    catch (\Exception $e) {
      $error = $this->t('TesseractOCR not found');
      $this->loggerFactory->get('ocr_image')->error($error);
    }

    $data = Normalizer::normalize(trim($data), Normalizer::FORM_C);
    $line_datas = array_filter(explode("\n", $data), function ($value) {
      return !empty(trim($value));
    });

    if (!empty($line_datas)) {
      // Set the full text.
      if (!empty($options['keyword_mode'])) {
        $data = $this->processTextKeywords($data);
      }
      $text['full_text'] = $data;

      // Set the line datas.
      $text['array'] = $line_datas;

      // Set the title.
      $title = trim(strip_tags(array_shift($line_datas)));
      $text['title'] = substr($title, 0, 512);

      // Set the alt.
      $alt = trim(strip_tags(implode(' ', $line_datas)));
      $text['alt'] = substr($alt, 0, 1024);
    }
    return $text;
  }

  /**
   * {@inheritdoc}
   */
  public function processTextKeywords($text) {
    // Only keep characters and number.
    preg_match_all('/[\p{L}\p{N}]{3,}/u', $text, $matches);
    // Count the frequency of each word.
    $wordCounts = array_count_values($matches[0]);
    // Sort by frequency.
    arsort($wordCounts);
    return implode(' ', array_keys($wordCounts));
  }

}
