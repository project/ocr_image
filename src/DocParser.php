<?php

namespace Drupal\ocr_image;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use LukeMadhanga\DocumentParser;
use PhpOffice\PhpPresentation\IOFactory as PowerpointIOFactory;
use PhpOffice\PhpPresentation\Shape\RichText;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Smalot\PdfParser\Parser;
use Symfony\Polyfill\Intl\Normalizer\Normalizer;
use thiagoalessio\TesseractOCR\TesseractOCR;

/**
 * Document parser service.
 */
class DocParser implements DocParserInterface {
  use StringTranslationTrait;

  /**
   * Constructor for OcrImage service.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Logger.
   * @param OcrImage $ocrImage
   *   OCR Image service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Message service.
   */
  public function __construct(protected readonly LoggerChannelFactoryInterface $loggerFactory, protected OcrImage $ocrImage, protected MessengerInterface $messenger) {
  }

  /**
   * {@inheritDoc}
   */
  public function getText(string $file, string $lang = 'eng', mixed $limit = 0, array $options = []) {
    $mimetype = mime_content_type($file);
    $ocr_image_service = $this->ocrImage;
    $messager = $this->messenger;
    switch ($mimetype) {
      case 'text/html':
      case 'text/plain':
      case 'application/rtf':
      case 'application/msword':
      case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
      case 'application/vnd.oasis.opendocument.text':
      case 'word/document.xml':
      case 'content.xml':
      case 'application/octet-stream':
        $data = '';
        try {
          $data = DocumentParser::parseFromFile($file);
        }
        catch (\Exception $e) {
          $messager->addError($this->t("Library PdfParser is not found"));
        }
        break;

      case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
      case 'application/vnd.ms-excel.sheet.macroEnabled.12':
      case 'application/vnd.ms-excel.template.macroEnabled.12':
      case 'application/vnd.ms-excel.sheet.binary.macroEnabled.12':
      case 'application/vnd.ms-excel':
        $data = '';
        $sheetData = [];
        try {
          $reader = IOFactory::createReaderForFile($file);
          $reader->setReadDataOnly(TRUE);
          $spreadsheet = $reader->load($file);
          $sheetData = $spreadsheet->getActiveSheet()
            ->toArray(NULL, TRUE, TRUE, TRUE);
        }
        catch (\Exception $e) {
          $messager->addError($this->t("Library PhpSpreadsheet is not found"));
        }
        foreach ($sheetData as $line) {
          $data .= trim(implode("\t", $line)) . PHP_EOL;
          if (!empty($limit) && strlen($data) > $limit) {
            break;
          }
        }
        break;

      case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
      case 'application/vnd.ms-powerpoint':
      case 'application/vnd.openxmlformats-officedocument.presentationml.slideshow':
        $data = '';
        $slides = [];
        try {
          $reader = PowerpointIOFactory::createReader($this->getPowerpointType($mimetype));
          $powerpoint = $reader->load($file);
          $slides = $powerpoint->getAllSlides();
        }
        catch (\Exception $e) {
          $messager->addError($this->t("Library PhpPresentation is not found"));
        }
        foreach ($slides as $slide_k => $slide_v) {
          $shapes = $slides[$slide_k]->getShapeCollection();
          foreach ($shapes as $shape_k => $shape_v) {
            $shape = $shapes[$shape_k];
            if ($shape instanceof RichText) {
              $paragraphs = $shapes[$shape_k]->getParagraphs();
              foreach ($paragraphs as $paragraph_v) {
                $text_elements = $paragraph_v->getRichTextElements();
                foreach ($text_elements as $text_element_v) {
                  $data .= trim($text_element_v->getText());
                  if (!empty($limit) && strlen($data) > $limit) {
                    break;
                  }
                }
              }
            }
          }
        }
        break;

      case 'image/gif':
      case 'image/jpeg':
      case 'image/bmp':
      case 'image/png':
        $data = '';
        try {
          $data = $ocr_image_service->getText($file, $lang);
        }
        catch (\Exception $e) {
          $error = $this->t('TesseractOCR not found');
          $messager->addError($error);
          $this->loggerFactory->get('ocr_image')->error($error);
        }
        break;

      case 'application/pdf':
        $data = '';
        try {
          $parser = new Parser();
          $document = $parser->parseFile($file);
          foreach ($document->getPages() as $page) {
            $data .= trim(str_replace(["\n\n", "\n"], " \n", $page->getText()));
            // Check Pdf is image pdf.
            $xobjects = $page->getXObjects();
            foreach ($xobjects as $image) {
              $type = $image->getHeader()->getDetails();
              if ('Image' === $type['Subtype'] && 'DCTDecode' === $type['Filter']) {
                $img_data = $image->getContent();
                if (empty($img_data)) {
                  continue;
                }
                $size = @getimagesizefromstring(base64_decode($img_data));
                try {
                  $ocr = (new TesseractOCR())->imageData($img_data, $size);
                  $dpi = $options['dpi'] ?? 300;
                  $data .= $ocr->lang($lang)->dpi($dpi)->run();
                }
                catch (\Exception $e) {
                  $messager->addError($this->t("TesseractOCR not found"));
                }
                /*
                if (!empty($limit) && mb_strlen($data) > $limit) {
                break;
                }
                 */
              }
            }
          }
        }
        catch (\Exception $e) {
          $messager->addError($this->t("PHP Parser not found"));
        }
        $data = preg_replace('/[^\PCc^\PCn^\PCs]/u', '', $data);
        break;

    }
    $checkData = Normalizer::normalize(trim($data), Normalizer::FORM_C);
    $data = str_replace("\t", '', empty($checkData) ? $data : $checkData);
    if (!empty($limit) && mb_strlen($data) > $limit) {
      $data = mb_substr($data, 0, $limit);
    }
    if (!empty($options['keyword_mode'])) {
      $data = $this->processTextKeywords($data);
    }
    $line_datas = array_filter(explode("\n", $data), function ($value) {
      return !empty(trim($value));
    });
    return $line_datas;
  }

  /**
   * {@inheritdoc}
   */
  protected function getPowerpointType($type): string {
    switch ($type) {
      case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
        $readerType = 'PowerPoint2007';
        break;

      case 'application/vnd.openxmlformats-officedocument.presentationml.slideshow':
      case 'application/vnd.ms-powerpoint':
        $readerType = 'PowerPoint97';
        break;

    }
    return $readerType;
  }

  /**
   * {@inheritdoc}
   */
  public function processTextKeywords($text) {
    // Only keep characters and number.
    preg_match_all('/[\p{L}\p{N}]{3,}/u', $text, $matches);
    // Count the frequency of each word.
    $wordCounts = array_count_values($matches[0]);
    // Sort by frequency.
    arsort($wordCounts);
    return implode(' ', array_keys($wordCounts));
  }

}
