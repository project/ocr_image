<?php

namespace Drupal\ocr_image\Plugin\Action;

use Drupal\Core\Action\Attribute\Action;
use Drupal\Core\Action\Plugin\Action\Derivative\EntityDeleteActionDeriver;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;

/**
 * Action description.
 */
#[Action(
  id: 'ocr_image_bulk_update_images',
  action_label: new TranslatableMarkup('Update empty image text (Image OCR)'),
)]
class BulkUpdateImages extends ViewsBulkOperationsActionBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {

    $entity_type = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    $form_mode = 'default';

    // Get the fields for the entity.
    $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type, $bundle);

    $ocr_image_service = \Drupal::service('ocr_image.OcrImage');
    $streamManager = \Drupal::service('stream_wrapper_manager');
    $entityTypeManager = \Drupal::entityTypeManager();
    // Get the entity form settings.
    $form_display = $entityTypeManager->getStorage('entity_form_display')
      ->load($entity_type . '.' . $bundle . '.' . $form_mode);

    // Loop over the fields looking for images.
    foreach ($fields as $field) {
      if ($field->getType() == 'image') {
        $field_name = $field->getName();
        $specific_widget_type = $form_display->getComponent($field_name);

        // Check if it is using the form widget.
        if ($specific_widget_type['type'] == 'ocr_image') {
          $new_full_text = '';
          $language = $specific_widget_type['settings']['language'];
          $limit = $specific_widget_type['settings']['limit'];

          // Load the image field.
          $image_field = $entity->get($field_name);

          // Loop through each of the images in the field.
          foreach ($image_field as &$image) {
            // Get the image file path.
            $fid = $image->target_id;
            $file = $entityTypeManager->getStorage('file')->load($fid);
            $uri = $file->getFileUri();
            $stream_wrapper_manager = $streamManager->getViaUri($uri);
            $file_path = $stream_wrapper_manager->realpath();

            // Get the text from the image.
            $image_text = $ocr_image_service->getText($file_path, $language, $limit);
            $new_alt = $image_text['alt'] ?? '';
            $new_title = $image_text['title'] ?? '';
            $full_text = $image_text['full_text'] ?? '';
            $new_full_text .= $full_text . PHP_EOL;

            // Get the current alt text.
            $alt = $image->alt;
            // Check if the alt text is empty.
            if (is_null($alt) || empty($alt)) {
              // Set the alt text.
              $image->set('alt', $new_alt);
            }

            // Get the current title text.
            $title = $image->title;
            // Check if the title text is empty.
            if (is_null($title) || empty($title)) {
              // Set the title text.
              $image->set('title', $new_title);
            }
          }

          // Deal with the full text field.
          $full_text_field = $specific_widget_type['settings']['text_field'];
          $current_full_text = $entity?->$full_text_field->value;
          if (empty($current_full_text)) {
            // Set the full text field.
            $entity->set($full_text_field, $new_full_text);
          }
        }
      }
    }

    $entity->save();

    return $this->t('All selected image texts have been updated');
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    // If certain fields are updated, access should be checked against them as
    // well. @see Drupal\Core\Field\FieldUpdateActionBase::access().
    return $object->access('update', $account, $return_as_object);
  }

}
