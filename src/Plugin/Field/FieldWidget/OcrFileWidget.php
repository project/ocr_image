<?php

namespace Drupal\ocr_image\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;
use Drupal\ocr_image\DocParserInterface;
use Drupal\ocr_image\Plugin\Field\TesseractLanguages;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Ocr pdf' widget.
 */
#[FieldWidget(
  id: 'ocr_pdf',
  label: new TranslatableMarkup('Ocr/parser file'),
  field_types: ['file'],
)]
class OcrFileWidget extends FileWidget {
  use Base;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Render\ElementInfoManagerInterface $element_info
   *   The element info manager service.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapperManager
   *   The stream wrapper manager service.
   * @param \Drupal\ocr_image\DocParserInterface $docParser
   *   Document parser service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ElementInfoManagerInterface $element_info, protected StreamWrapperManagerInterface $streamWrapperManager, protected DocParserInterface $docParser) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings, $element_info);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('element_info'),
      $container->get('stream_wrapper_manager'),
      $container->get('ocr_image.DocParser'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $field_name = $items->getName();
    $field_text_name = $this->getSetting('text_field');
    $fids = $form_state->getValue($field_name);
    $fid = current($fids);
    if (!empty($fids[$delta]) && empty($fids[$delta]["description"])) {
      if (is_array($fids[$delta])) {
        $fid = $fids[$delta];
        if (is_array($fid) && !empty($fid["fids"])) {
          $fid = end($fid["fids"]);
        }
      }
      $file = File::load($fid);
      $uri = $file->getFileUri();
      $stream_wrapper_manager = $this->streamWrapperManager->getViaUri($uri);
      $file_path = $stream_wrapper_manager->realpath();
      $language = $this->getSetting('language');
      if (empty($language)) {
        $language = TesseractLanguages::tesseractConvertLang();
      }
      $options = [
        'keyword_mode' => $this->getSetting('keyword_mode'),
        'dpi' => 300,
      ];
      $line_datas = $this->docParser->getText($file_path, $language, $this->getSetting('limit'), $options);
      if (!empty($line_datas)) {
        $ocr = implode(" " . PHP_EOL, $line_datas);
        if (!empty($field_text_name)) {
          $form_state->setValue($field_text_name, $ocr);
        }
        $element["#default_value"]["description"] = strip_tags($ocr);
        $element['#attached']['drupalSettings']['ocr_field_widget'] = [
          'field_name' => $field_name,
          'field' => $field_text_name,
          'ocr' => $ocr,
        ];
        $element['#attached']['library'][] = 'ocr_image/ocr_image';
      }
    }

    return $element;
  }

}
