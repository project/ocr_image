<?php

namespace Drupal\ocr_image\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Entity\File;
use Drupal\image\Plugin\Field\FieldWidget\ImageWidget;
use Drupal\ocr_image\DocParserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Ocr image' widget.
 */
#[FieldWidget(
  id: 'ocr_image',
  label: new TranslatableMarkup('Ocr image'),
  field_types: ['image'],
)]
class OcrImageWidget extends ImageWidget {
  use Base;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Render\ElementInfoManagerInterface $element_info
   *   The element info manager service.
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   The image factory service.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapperManager
   *   The stream wrapper manager service.
   * @param \Drupal\ocr_image\DocParserInterface $docParser
   *   Document parser service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ElementInfoManagerInterface $element_info, ImageFactory $image_factory, protected StreamWrapperManagerInterface $streamWrapperManager, protected DocParserInterface $docParser) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings, $element_info, $image_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('element_info'),
      $container->get('image.factory'),
      $container->get('stream_wrapper_manager'),
      $container->get('ocr_image.OcrImage'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $field_name = $items->getName();
    $field_text_name = $this->getSetting('text_field');
    $language = $this->getSetting('language');
    $fid = $form_state->getValue($field_name);
    if (!empty($fid)) {
      if (is_array($fid)) {
        $fid = end($fid);
        if (is_array($fid) && !empty($fid["fids"])) {
          $fid = end($fid["fids"]);
        }
      }
      $file = File::load($fid);
      $uri = $file->getFileUri();
      $stream_wrapper_manager = $this->streamWrapperManager->getViaUri($uri);
      $file_path = $stream_wrapper_manager->realpath();
      $options = [
        'keyword_mode' => $this->getSetting('keyword_mode'),
        'dpi' => 300,
      ];
      $image_text = $this->docParser->getText($file_path, $language, $this->getSetting('limit'), $options);
      $full_text = $image_text['full_text'];
      if (!empty($full_text)) {
        if (!empty($field_text_name)) {
          $form_state->setValue($field_text_name, $full_text);
        }
        $element["#default_value"]["alt"] = $image_text['alt'];
        $element["#default_value"]["title"] = $image_text['title'];
        $element['#attached']['drupalSettings']['ocr_field_widget'] = [
          'field_name' => $field_name,
          'field' => $field_text_name,
          'ocr' => $full_text,
        ];
        $element['#attached']['library'][] = 'ocr_image/ocr_image';
      }
    }

    return $element;
  }

}
