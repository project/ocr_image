<?php

namespace Drupal\ocr_image\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\ocr_image\Plugin\Field\TesseractLanguages;

/**
 * Class for setting widget field file.
 */
trait Base {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'text_field' => '',
      'language' => 'eng',
      'limit' => 512,
      'keyword_mode' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $options = [];
    $entity_type = $form["#entity_type"];
    $bundle = $form["#bundle"];
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $fieldDefinitions = $entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    $userInput = $form_state->getUserInput();
    $typSupport = [
      'text',
      'text_long',
      'text_with_summary',
      'text_textarea_with_summary',
      'string',
      'string_long',
      'string_textfield',
      'string_textarea',
    ];
    if (!empty($userInput["fields"])) {
      foreach ($userInput["fields"] as $field_name => $field_widget) {
        if (!empty($field_widget['type']) &&
          in_array($field_widget['type'], $typSupport)) {
          $options[$field_name] = (string) $fieldDefinitions[$field_name]->getLabel();
        }
      }
    }
    $elements['text_field'] = [
      '#title' => $this->t('Text field'),
      '#description' => $this->t('To store ocr text'),
      '#default_value' => $this->getSetting('text_field'),
      '#type' => 'select',
      '#options' => $options,
      '#empty_option' => $this->t('- None -'),
    ];
    $elements['language'] = [
      '#title' => $this->t('Language OCR'),
      '#description' => $this->t('Languages/Scripts supported'),
      '#default_value' => $this->getSetting('language'),
      '#type' => 'select',
      '#options' => TesseractLanguages::tesseractLangues(),
      '#empty_option' => $this->t('- None -'),
    ];
    $elements['limit'] = [
      '#title' => $this->t('Limited number of characters'),
      '#description' => $this->t('Useful for summary document, leave blank if unlimited'),
      '#default_value' => $this->getSetting('limit'),
      '#type' => 'number',
    ];
    $elements['keyword_mode'] = [
      '#title' => $this->t('Remove all duplicate word'),
      '#description' => $this->t('Useful for index document'),
      '#default_value' => $this->getSetting('keyword_mode'),
      '#type' => 'checkbox',
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Text field: @text_field', ['@text_field' => $this->getSetting('text_field')]);
    if (!empty($this->getSetting('language'))) {
      $summary[] = $this->t('Language: @language', ['@language' => $this->getSetting('language')]);
    }
    if (!empty($this->getSetting('limit'))) {
      $summary[] = $this->t('Limited: @limit', ['@limit' => $this->getSetting('limit')]);
    }
    return $summary;
  }

}
