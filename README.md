# OCR Image

This module will convert image to text into a link field
use library tesseract and pdfparser

For a full description of the module, visit the
[project page](https://www.drupal.org/project/ocr_image).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/ocr_image).

## This module has a service that can be used by your own module

For example:

```
$file_path = 'https://example.com/photo.jpg';
$language = 'eng';
$limit = 500;
$ocr_image_service = \Drupal::service('ocr_image.OcrImage');
$image_text = $ocr_image_service->getText($file_path, $language, $limit);
```
This will return an array with the following keys: full_text (everything,
as it appears on the image), title (only the first line), alt
(everything but the first line) and array (1 line of text per value).

## Update all existing images
This requires using View Bulk Operations.

1) Optionally add a text field to the entity that your image field belongs to.
2) Go to the "Manage Form Display" tab for the entity with the image field.
3) Change the widget to OCR Image. Configure the widget as desired.
4) Create a view that lists the entities with the image field.
Add a the Bulk Operations field.
5) Save the view.
6) Now use it to select all your entities and choose the
"Update empty image text (Image OCR)"
