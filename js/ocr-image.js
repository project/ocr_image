(function ($, Drupal, drupalSettings) {
  'use strict';
  let field = drupalSettings.ocr_field_widget.field;
  let ocr = drupalSettings.ocr_field_widget.ocr;
  if(field != '' && $("input[name*='" + field + "[0][value]']").length){
    ocr = $.trim($("input[name*='" + field + "[0][value]']").val() + ' ' + ocr);
    $("input[name*='" + field + "[0][value]']").val(ocr);
  }
  if(field != '' && $("textarea[name*='" + field + "[0][value]']").length){
    ocr = $.trim($("textarea[name*='" + field + "[0][value]']").val() + ' ' + ocr);
    $("textarea[name*='" + field + "[0][value]']").val(ocr);
    let editorId = $("textarea[name*='" + field + "[0][value]']").prop("id");
    if(typeof CKEDITOR !== 'undefined' && CKEDITOR.instances[editorId] != undefined){
      let editor = CKEDITOR.instances[editorId].setData(ocr);
    }
    if (typeof CKEditor5 !== 'undefined') {
      editorId = $("textarea[name*='" + field + "[0][value]']").data("ckeditor5-id");
      if (typeof editorId === "object") {
        editorId = editorId.toString();
      }
      let editorInstance = Drupal.CKEditor5Instances.get(editorId);
      if (editorInstance) {
        editorInstance.setData(ocr);
      }
    }
  }
})(jQuery, Drupal, drupalSettings);
